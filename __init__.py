from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from ctx import Context
import leds
from st3m.ui import colours


class Flashlight(Application):
    input = InputController()

    brightness: int = 3  # 0-5
    col_value: int = 153
    led_value: float = 0.6
    is_exiting: bool = False

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def on_exit(self) -> None:
        self.is_exiting = True
        for i in range(0, 40):
            leds.set_rgb(i, 0, 0, 0)
        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.is_exiting:
            return

        self.input.think(ins, delta_ms)

        if self.input.buttons.app.left.pressed:
            if self.brightness > 0:
                self.brightness = self.brightness - 1
        elif self.input.buttons.app.right.pressed:
            if self.brightness < 5:
                self.brightness = self.brightness + 1

        self.col_value = int(255.0 / 5.0 * self.brightness)
        self.led_value = 0.2 * self.brightness

    def draw(self, ctx: Context) -> None:
        if self.is_exiting:
            return

        ctx.rectangle(-120, -120, 240, 240)

        ctx.rgb(self.col_value, self.col_value, self.col_value)
        ctx.fill()

        for i in range(0, 40):
            leds.set_rgb(i, self.led_value, self.led_value, self.led_value)
        leds.update()

        ctx.save()
        ctx.rgb(*colours.BLACK)
        ctx.text_baseline = ctx.MIDDLE
        ctx.text_align = ctx.CENTER
        ctx.move_to(0, 20)
        ctx.font_size = 60
        ctx.font = "Material Icons"
        ctx.text("\ue430")
        ctx.restore()
